//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
package com.thecoverofnight.memmap;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import com.google.common.primitives.UnsignedBytes;
import com.google.common.primitives.UnsignedInteger;
import com.google.common.primitives.UnsignedLong;

public class MemRange {

	public long end;
	public long start;
	public String filename;
	public String base_dir;
	public RandomAccessFile fhandle;
	public File file;
	public long fsize;
	private long pos;

	
	public MemRange (long start2, long end2, String filename, String base_dir) throws FileNotFoundException {
		this.start = start2;
		this.end = end2;
		this.file = new File(base_dir, filename);
		this.filename = filename;
		this.base_dir = base_dir;
		this.fhandle = new RandomAccessFile (file, "r");
		this.fsize = file.length();
		this.pos = 0;
	}
	
	public void seek_to (long offset) throws IOException {
		if (in_range(offset+start)) {
			this.fhandle.seek(offset);
			this.pos = offset;
		}
	}
	
	public boolean in_range(long addr) {
		return start <= addr && addr <= end;
	}
	
	public boolean can_read (long offset, long len) {
		return in_range(start + (offset+len));
	}
	
	public long calc_offset(long addr) {
		if (!in_range(addr))
			return -1;
		return addr - start;
	}

	public long get_pos_as_addr(long pos) {
		if (in_range(start+pos))
			return start + pos;
		return -1;
	}
	
	public String get_full_path() {
		return file.toString();
	}
	
	public String toString () {
		return String.format("0x%08x-0x%08x", start, end);
	}
	
	public byte[] read(long off) {
		return read (off, 1);
	}
	public byte[] read(long off, long size) {
		byte[] n_res = new byte[0];
		if (off+size > fsize)
			return n_res;
		
		byte [] res = new byte[(int) size];
		try {
			pos = off;
			fhandle.seek(off);
			long a_sz = fhandle.read(res);
			n_res = a_sz < res.length ? new byte[(int) a_sz ] : new byte[(int) size];
			pos += a_sz;
			int n = 0;
			while (n < n_res.length){
				n_res[n] = res[n];
				n+=1;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return n_res;
	}
		
	public long read_dword(boolean littleendian) {
		return read_dword_at_off(pos, littleendian);
	}
	
	public long read_dword_at_off (long off) {
		return read_dword_at_off(off, true);
		
	}
	
	public long read_dword_at_off (long off, boolean littleendian) {
		byte [] bytes = read(off, 4);
		if (bytes.length < 4)
			return UnsignedInteger.MAX_VALUE.longValue();
		if (littleendian)  return to_unsignedlong_le(bytes);
		return to_unsignedlong_be(bytes);
	}
	
	
	public long read_qword_at_addr(long addr, boolean littleendian) {
		return read_qword_at_off(addr-start);
		
	}
	
	public long read_qword_at_addr(long addr) {
		return read_qword_at_off(addr-start, true);
	}
	
	public long read_qword() {
		return read_qword(true);
	}
	
	public long read_qword(boolean littleendian) {
		return read_qword_at_off(pos, littleendian);
	}
	
	public long read_qword_at_off (long off) {
		return read_qword_at_off(off, true);
	}
	
	public long read_qword_at_off (long off, boolean littleendian) {
		byte [] bytes = read(off, 8);
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		if (littleendian) bb.order( ByteOrder.LITTLE_ENDIAN);
		if (bytes.length < 8)
			return UnsignedLong.MAX_VALUE.longValue();
		return UnsignedLong.valueOf(bb.getLong()).longValue(); 
	}
	
	
	public long read_dword_at_addr(long addr, boolean littleendian) {
		return read_qword_at_off(addr-start);
		
	}
	
	public long read_dword_at_addr(long addr) {
		return read_qword_at_off(addr-start, true);
	}
	
	public long read_dword() {
		return read_qword(true);
	}
		
	public static MemRange in_ranges(long addr, List<MemRange> ranges, MemRange range) {
		if (range != null && range.in_range(addr))
			return range;
		
		for (MemRange r: ranges) {
			if (r.in_range(addr)) {
				return r;
			}
		}
		return null;
	}
	
	public static List<MemRange> produce_ranges(String dumps_dir) throws FileNotFoundException {
		List<MemRange> ranges = new ArrayList<MemRange>();
		File folder = new File(dumps_dir);
		File[] listOfFiles = folder.listFiles();
		
		ArrayList<File> dump_files = new ArrayList<File>();
	    for (int i = 0; i < listOfFiles.length; i++) {
	    	if (listOfFiles[i].isFile() && listOfFiles[i].toString().endsWith(".bin")) {
	    		dump_files.add(listOfFiles[i]);
	    	} 
	    }
		
	    for (File f : dump_files) {
	    	String base_dir = f.getParent();
	    	String filename = f.getName();
	    	String h_range = filename.split(".bin")[0];
	    	String h_start = h_range.split("-")[0];
	    	String h_end = h_range.split("-")[1];
	    	long start = UnsignedLong.valueOf(h_start.replace("0x", ""), 16).longValue();
	    	long end = UnsignedLong.valueOf(h_end.replace("0x", ""), 16).longValue();
	    	MemRange m = new MemRange(start, end, filename, base_dir);
	    	ranges.add(m);
	    }
		return ranges;
	}

	public long to_unsignedlong_le (byte[] bytes) {
	    Long value = (long) bytes[0] & 0xFF;
	    value |= (long) ( (bytes[1]& 0xFF) << 8) & 0x0FFFF;
	    value |= (long) ( (bytes[2]& 0xFF) << 16) & 0x0FFFFFF;
	    value |= (long) ( (bytes[3]& 0xFF) << 24) & 0x0FFFFFFFFL;
	    return value;
	}
	
	public long to_unsignedlong_be (byte[] bytes) {
	    Long value = (long) (bytes[3] & 0xFF);
	    value |= (long)  ((bytes[2]&0xFF) << 8) & 0x0FFFF;
	    value |= (long)  ((bytes[1]&0xFF) << 16) & 0x0FFFFFF;
	    value |= (long)  ((bytes[0]&0xFF) << 24) & 0x0FFFFFFFFL;
	    return value;
	}
	
	public static MemRange find_value_range (long addr, List<MemRange> ranges) {
		MemRange e = null;
		for (MemRange r : ranges) {
			if (r.in_range(addr)) {
				e = r;
				break;
			}
		}
		return e;
	}
}
