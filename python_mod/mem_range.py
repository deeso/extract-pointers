#Copyright 2015 Adam Pridgen
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

import os,struct

def dump_r2_loader(ranges, r2_loader_script):
    import os
    r2_loader_out = open (r2_loader_script, 'w')
    _ranges = {}

    for r in ranges:
        if r.fsize == 0:
            continue
        _ranges[r.start] = r

    sorted_ranges = _ranges.keys()
    mapped_load_fmt = "on %s 0x%08x"
    for addr in sorted_ranges:
        filename = _ranges[addr].filename
        base_dir = _ranges[addr].base_dir
        filename = os.path.join(base_dir, filename)
        # dump chunks
        load_str = mapped_load_fmt% (filename, addr)
        r2_loader_out.write (load_str+'\n')

def in_ranges (addr, ranges, range_ = None):
    if not range_ is None and range_.in_range(addr):
        return range_

    for r in ranges:
        if r.in_range(addr):
            return r
    return None

def produce_ranges(dumps_dir):
    ranges = [i for i in os.listdir(dumps_dir) if i.find(".bin") > -1 and i.find("-") > -1]
    ranges_values = []
    for r in ranges:
        range_ = r.split(".bin")[0]
        start = long(range_.split('-')[0], 16)
        end = long(range_.split('-')[1], 16)
        d = Range (start, end, r, dumps_dir, load_data=True)
        ranges_values.append(d)
    return ranges_values

def find_value_range (value, ranges):
    for i in ranges:
        if (i.in_range(value)):
            return i
    return None


class Range (object):
    def __init__ (self, start, end, filename,
                    base_dir = None, load_data=False,
                    data = None):
        self.start = start
        self.end = end
        self.filename = filename
        self.base_dir = base_dir
        #self.fhandle = open(os.path.join(base_dir, filename), "rb")
        self.fdata = None
        self.fsize = 0
        if load_data:
            self.fdata = open(os.path.join(base_dir, filename), "rb").read()
        elif not data is None:
            self.fdata = data
        if self.fdata:
            self.fsize = len(self.fdata)#self.get_size()
        self.pos = 0

    def get_size(self):
        return self.fsize

    def in_range (self, value):
        return self.start <= value and value <= self.end

    def can_read (self, offset, len_):
        if not self.in_range(self.start+offset):
            return False
        if self.pos+len_ >= (self.end-self.start):
            return False
        return True

    def calc_offset (self, addr):
        if self.in_range(addr):
            return addr - self.start
        return -1

    def get_pos_as_addr (self):
        return self.start + self.pos

    def get_full_path (self):
        if not self.base_dir is None:
            return os.path.join (self.base_dir, self.filename)
        return self.filename

    def __str__ (self):
        #return "filename: %s start: 0x%08x end: 0x%08x"%(self.filename, self.start, self.end)
        return "0x%08x-0x%08x"%(self.start, self.end)

    def read_at_addr(self, addr, size):
        if not self.in_range(addr):
            return None
        pos = addr - self.start
        return self.read(pos, size)

    def read_dword(self, addr=None, offset=None, littleendian = True):
        if not addr is None:
            return self.read_dword_at_addr(addr, littleendian)
        elif not offset is None:
            return self.read_dword_at_offset(offset, littleendian)
        else:
            return self.read_dword_at_offset(self.pos, littleendian)

    def read_dword_at_addr(self, addr, littleendian=True):
        if not self.in_range(addr):
            return None
        pos = addr - self.start
        return self.read_dword_at_offset(pos, littleendian)

    def read_dword_at_offset(self, offset, littleendian=True):
        if not self.in_range(offset+self.start) or\
            (offset != self.pos and not self.seek_to(offset)):
            return None
        result = self.read(offset, 4, )
        if len(result) != 4:
            return None
        if littleendian:
            return struct.unpack("<I", result)[0]
        else:
            return struct.unpack(">I", result)[0]

    def read_qword(self, addr=None, offset=None, littleendian = True):
        if not addr is None:
            return self.read_qword_at_addr(addr, littleendian)
        elif not offset is None:
            return self.read_qword_at_offset(offset, littleendian)
        else:
            return self.read_qword_at_offset(self.pos, littleendian)

    def read_qword_at_addr(self, addr, littleendian=True):
        if not self.in_range(addr):
            return None
        pos = addr - self.start
        return self.read_qword_at_offset(pos, littleendian)

    def read_qword_at_offset(self, offset, littleendian=True):

        if not self.in_range(offset+self.start) or\
            (offset != self.pos and not self.seek_to(offset)):
            return None
        result = self.read(offset, 8)
        if littleendian:
            return struct.unpack("<Q", result)[0]
        else:
            return struct.unpack(">Q", result)[0]

    def _read (self, size=1, pos=None):
        if pos is None:
            pos=self.pos
        r = self.fdata[pos:pos+size]#self.fhandle.read(size)
        self.pos = pos+len(r)
        return r

    def read (self, pos, sz =1 ):
        return self._read(sz, pos)

    def seek_to (self, offset):
        if not self.in_range(offset+self.start):
            return False
        self.pos = offset
        #self.fhandle.seek(offset)
        return True

    def read_all_as_dword (self, little_endinan=True):
        struct_fmt = "<%dI"%(self.fsize/4)
        if not little_endinan:
            struct_fmt = ">%dI"%(self.fsize/4)
        dwords = struct.unpack(struct_fmt, self.fdata)
        return dwords

    def read_all_as_qword (self, little_endinan=True):
        struct_fmt = "<%dQ"%(self.fsize/4)
        if not little_endinan:
            struct_fmt = ">%dQ"%(self.fsize/4)
        qwords = struct.unpack(struct_fmt, self.fdata)
        return qwords
