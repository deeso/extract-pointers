# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This is a set of tools that can be used to extract pointers from a set of contiguous memory blocks and import pointers into redis.  There is also an import functionality for strings.  In order to speed up the import process,
a python script (see python_mod/) was written to perform the imports of the data in parallel.   Depending on the systems running the scripts and Java extraction, the number of connections may need to be tweaked in the Python scripts and the Java source code.

### How do I get set up? ###

* Extract the Java Process memory using the Volatility.
* Install Maven, Java, and Eclipse
* Import the ptr_scan Java project into Eclipse and use maven to compile the target application
* Use the python scripts to perform the extraction of the pointers to import the data into redis

### Contribution guidelines ###

* No outside contributions accepted.

### Who do I talk to? ###

* Adam "deeso or dso" Pridgen
