#Copyright 2015 Adam Pridgen
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

from subprocess import Popen, PIPE
from mem_range import (Range, produce_ranges, in_ranges, find_value_range)
from mem_range import produce_ranges, in_ranges, find_value_range
import os,sys,redis,struct,threading,time
from datetime import datetime

import sys
from subprocess import PIPE, Popen
from threading  import Thread

try:
    from Queue import Queue, Empty
except ImportError:
    from queue import Queue, Empty

COMPLETED_PROCESSES = 0
ALLOWED_PROCESSES = 50
PROCESSES = []
ON_POSIX = 'posix' in sys.builtin_module_names
PTR_SCAN_JAR = None

def enqueue_output(out, queue):
    for line in iter(out.readline, b''):
        queue.put(line)
    try:
        out.close()
    except:
        pass

#example command:
# python perform_ptr_scan.py <ptr_scan> <dump_dir> <range_addr> <offset> <end> <word_sz> <littleendian> <host> <port>
# python perform_ptr_scan.py ~/ptr_scan.jar ~/java_ubuntu_custom2/java_dumps/ 0x93e9f000 0 0 4 1 10.16.121.14 6379

CARVING_SPACE = 1000000

# python perform_ptr_scan.py ~/ptr_scan.jar ~/java_ubuntu_custom2/java_dumps/ 0x93e9f000 0 0 4 1 10.16.121.14 6379
def start_ptr_scan_range (dump_dir, range_, word_sz, littleendian, host, port, namespace, offset=0, end=0):
    global PROCESSES, COMPLETED_PROCESSES, ALLOWED_PROCESSES
    start = 0
    r_addr = range_.start
    base = range_.start
    le_d = 1 if littleendian else 0
    cnt = 0
    fsize = range_.fsize
    s = str(range_)
    end_of_pos = end if end > 0 else fsize
    if fsize < 1000000 and offset == 0:
        cmd = ["java", "-jar", PTR_SCAN_JAR, "ptrscan", "%s"%dump_dir,  "0x%08x"%r_addr,
               "0x%08x"%start, "0x%08x"%end_of_pos, "%d"%word_sz, "%d"%le_d,
               "%s"%host, "%s"%port, namespace]
        #print cmd
        p = Popen(cmd, stdout=PIPE, bufsize=1, close_fds=ON_POSIX)
        q = Queue()
        t = Thread(target=enqueue_output, args=(p.stdout, q))
        t.daemon = True # thread dies with the program
        t.start()
        PROCESSES.append((p,q))
        print ("[%s] Started proc total = [%d] completed[%d] processes for %s size = 0x%08x bytes"%(str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y")), len(PROCESSES), COMPLETED_PROCESSES, s, range_.fsize))
        cnt += 1
    else:
        pos = offset
        to = CARVING_SPACE
        end_of_pos = end if end > 0 else fsize
        print ("The end value = %d end_pos = %d"%(end, end_of_pos))
        while pos < end_of_pos:
            cnt += 1
            to = 0
            if pos+CARVING_SPACE >= end_of_pos:
                to = fsize
            else:
                to = pos+CARVING_SPACE

            cmd = ["java", "-jar", PTR_SCAN_JAR, 'ptrscan', "%s"%dump_dir,  "0x%08x"%r_addr,
                   "0x%08x"%pos, "0x%08x"%to, "%d"%word_sz, "%d"%le_d,
                   "%s"%host, "%s"%port, namespace]
            #print cmd
            p = Popen(cmd, stdout=PIPE, bufsize=1, close_fds=ON_POSIX)
            q = Queue()
            t = Thread(target=enqueue_output, args=(p.stdout, q))
            t.daemon = True # thread dies with the program
            t.start()
            PROCESSES.append((p,q))
            print ("[%s] Started proc total = [%d] completed[%d] processes for %s from = 0x%08x to = 0x%08x"%(str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y")), len(PROCESSES), COMPLETED_PROCESSES, s, base+pos, pos+base+CARVING_SPACE))
            pos += CARVING_SPACE
            if len(PROCESSES) > ALLOWED_PROCESSES:
                prune_processes()

    range_str = "0x%08x-0x%08x"%(range_.start, range_.end)
    return cnt


def prune_processes ( ):
    global PROCESSES, ALLOWED_PROCESSES, COMPLETED_PROCESSES
    tot = len(PROCESSES)
    print ("[%s] Pruning processes  total = [%d] completed[%d] processes "%(str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y")), tot, COMPLETED_PROCESSES, ))
    while len(PROCESSES) >= ALLOWED_PROCESSES:
        cnt = 1
        new_running_processes = []
        for p,q in PROCESSES:
            try:
                line = q.get_nowait()
                COMPLETED_PROCESSES += 1
                try:
                    p.communicate()
                except:
                    pass
                #p.communicate()
                p.wait()
            except Empty:
                new_running_processes.append((p,q))
        PROCESSES = new_running_processes
        if len(PROCESSES) >= ALLOWED_PROCESSES:
            if cnt % 60 == 0:
                tot = len(PROCESSES)
                print ("[%s] Waiting on some processes to complete slept for (%d)s total = [%d] completed[%d] "%(str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y")), cnt, tot, COMPLETED_PROCESSES, ))
            time.sleep(1)
            cnt += 1
    #return running_processes


if __name__ == "__main__":
    print sys.argv[1:]
    PTR_SCAN_JAR = sys.argv[1]
    dumps_dir = sys.argv[2]
    word_sz = int(sys.argv[3])
    littleendian = True if sys.argv[4] == '1' else False
    host = sys.argv[5]
    port = int(sys.argv[6])
    namespace = sys.argv[7]
    r_addr = int(sys.argv[8], 16)
    offset = int(sys.argv[9], 16)
    end = int(sys.argv[10],16)
    print ("The end %08x and start %08x"%(end, offset))
    start_time = datetime.now().strftime("%H:%M:%S %m-%d-%Y")
    print ("[%s] Starting... "%start_time)
    ranges = produce_ranges (dumps_dir)
    range_ = find_value_range (r_addr, ranges)
    cnt_ = start_ptr_scan_range (dumps_dir, range_, word_sz, littleendian, host, port, namespace, offset=offset, end=end)
    cnt = len(PROCESSES)
    print "Size of the range is:%d"%range_.fsize
    print ("[%s] All scans started, waiting for the completion now"%str(datetime.now().strftime("%H:%M:%S %m-%d-%Y")))
    cnt = 0
    while len(PROCESSES) > 0:
        new_running_processes = []
        for p,q in PROCESSES:
            try:
                line = q.get_nowait()
                COMPLETED_PROCESSES += 1
                try:
                    p.communicate()
                except:
                    pass
                #p.communicate()
                p.wait()
            except Empty:
                new_running_processes.append((p,q))
        PROCESSES = new_running_processes
        if cnt % 120 == 0:
            print ("[%s] Waiting on some %d to complete, completed=[%d] sleeping"%(datetime.now().strftime("%H:%M:%S %m-%d-%Y"),len(PROCESSES), COMPLETED_PROCESSES))
        time.sleep(.5)
        cnt += 1

    end_time = datetime.now().strftime("%H:%M:%S %m-%d-%Y")
    print ("[%s] All scans completed."%end_time)
    print ("Start time = %s"%start_time)
    print ("End time = %s"%end_time)
