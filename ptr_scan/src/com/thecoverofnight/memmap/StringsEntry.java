//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
package com.thecoverofnight.memmap;

import java.io.File;
import java.io.FileNotFoundException;

import com.google.common.primitives.UnsignedLong;

public class StringsEntry {
	
	public long addr;
	public String string;
	
	public StringsEntry (String string, long vaddr){
		this.string = string;
		this.addr = vaddr;
	}
	
	public static StringsEntry createStringEntry(String line) throws FileNotFoundException {
		String data = "";
		long base_vaddr = -1, offset = -1;
		String[] line_split = line.split(" ");
		if (line_split.length < 3)
			return null;
		// handle the offset
		String [] rem = line.split(": ");
		if (rem.length < 2)
			return null;
		
		String str_offset = rem[1];
		str_offset = str_offset.trim();
		if (str_offset.split(" ").length < 2) {
			return null;
		}
		
		str_offset = str_offset.split(" ")[0];
		
		
		rem = line.split(str_offset+" ");
		if (rem.length > 1){
			// FIXME if the string contains "<offset> " then there will be more than
			// 2 elements in the array
			data = rem[1];
		}
		String fname = line_split[0];
		File file = new File(fname);
		String filename = file.getName();
		String h_range = filename.split(".bin")[0];
		String h_start = h_range.split("-")[0];
		base_vaddr = UnsignedLong.valueOf(h_start.replace("0x", ""), 16).longValue();
		offset = UnsignedLong.valueOf(str_offset, 16).longValue();
		return new StringsEntry(data, base_vaddr+offset);
	}
}
