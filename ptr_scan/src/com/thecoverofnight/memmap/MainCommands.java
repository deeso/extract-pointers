//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

package com.thecoverofnight.memmap;

import java.io.IOException;

public class MainCommands {
	public static void do_ptr_scan(String[] args) throws IOException, InterruptedException {
		PtrScan.do_ptr_scan(args);
	}
	public static void do_strings_to_redis(String[] args) throws IOException {
		StringsToRedis.do_strings(args);
	}
	
	
	public static void main(String[] args) throws IOException, InterruptedException {
		String [] args2 = new String[args.length-1];
		int pos = 1;
		while (pos < args.length) {
			args2[pos-1] = args[pos];
			pos += 1;
		}

		if (args[0].equalsIgnoreCase("ptrscan")){
			do_ptr_scan(args2);
			return ;
		} else if (args[0].equalsIgnoreCase("str2red")){
			System.err.println("LOG Performing str2edit");
			do_strings_to_redis(args2);
			return;
		} else {
			/*System.err.print("Sent me: ");
			for (String arg: args) {
				System.err.print("arg: "+arg+ " ");
			}
			System.err.println("");
			*/
			System.err.println("ERROR No command given <ptrscan> or <str2red>");
			System.err.println("============<ptrscan>============");
			System.err.println("ptrscan <dumps_dir> <rangeAddr> <offset> <end> <word_sz> <littleendian> <host> <port> <namespace>");
			System.err.println("dumps_dir directory with dumps of form 0x<start>-0x<end>.bin");
			System.err.println("rangeAddr address range (file 0x<start>-0x<end>.bin) to start with");
			System.err.println("offset into the range (specify 0 for start)");
			System.err.println("end into the range (specify 0 for end)");
			System.err.println("word_sz into the range (specify (4 default or 8 bytes)");
			System.err.println("littleendian (specify 1 for littleendian)");
			System.err.println("host redis host");
			System.err.println("port redis port)");
			System.err.println("namespace redis namespace infront of redis keys)");
			System.err.println("============<re>============");
			System.err.println("str2red <stringsfile> <host> <port> <namespace>");
			System.err.println("strings file format: <filename with dump form 0x<start>-0x<end>.bin");
			System.err.println("host redis host");
			System.err.println("port redis port)");
			System.err.println("namespace redis namespace infront of redis keys)");
		}

	}
}

