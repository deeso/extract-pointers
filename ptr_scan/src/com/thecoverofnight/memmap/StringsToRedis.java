//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
package com.thecoverofnight.memmap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;


import com.google.common.primitives.UnsignedLong;

public class StringsToRedis {
	private static final int MAX_STRING_PROC_T = 150;
	private static final int MAX_REDI_SENDER = 25;
	private static final String ERROR_LOG = "/home/dso/ptr_scan_errors";
	private static final String REDIS_STRINGS = "strings";
	private static final String REDIS_STRINGS_SET_KEY = "strings";
	private static final String REDIS_ADDRS_SET_KEY = "addrs";
	private static final String REDIS_STRING_BY_ADDR_HSET = "stringsbyaddr";
	private static final String REDIS_STRING_BY_STR_HSET = "stringsbystr";
	
	private String namespace;
	private String stringFile;
	private RedisSubmit redis_sender;
	
	interface ProcessedStringCallback {
	    void addEntry(StringsEntry e); // would be in any signature
	}
	
	class StringProcessor extends Thread {
		boolean keeprunning = true;
		boolean stillRunning = false;
		boolean exitAfterSending = false;

		
		Stack<String> entries;
		private StringsToRedis cb;
		public StringProcessor(StringsToRedis stringsToRedis, Stack<String> lines) {
			this.cb = stringsToRedis;
			this.entries = lines;
		}
		
		void processStringsEntry() {
			String line = null;
			
			try {
				synchronized (entries) {
					if (entries.size() > 0)
						line = entries.pop();
				}
				if (line == null) return;

				addEntry(StringsEntry.createStringEntry(line));
			}catch (Exception ex) {
				
			}

		}
		
		public void run() {
			stillRunning = true;
			while (keeprunning) {
				processStringsEntry();	
				if (exitAfterSending && entries.size() == 0){
					keeprunning = false;
					continue;
				}
				try {
					if (entries.size() == 0 && !exitAfterSending)
						Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}	
			}
			stillRunning = false;
			
		}

	}
	
	class RedisSubmit extends Thread {
		
		Stack<StringsEntry> entries;// = new Stack<StringsEntry>();
		boolean keeprunning = true;
		boolean exitAfterSending = false;
		private Jedis redis_con;
		int port;
		String server;
		boolean stillRunning = false;
		public RedisSubmit(String redis, int port, Stack<StringsEntry> sharedStack) {
			this.server = redis;
			this.port = port;
			connect();
			this.entries = sharedStack;
		}
		
		public void connect () {
			this.redis_con = new Jedis(server, port);
		}
		
		public void reconnect() {
			try {
				redis_con.shutdown();
			} catch (Exception ex) {
				// swallow this one, it does not mean much
			}
			try {
				connect ();
			} catch (Exception ex) {
				// this one matters (do
				ex.printStackTrace();
				keeprunning = false;
				stillRunning = false;
				exitAfterSending = false;
				// rethrow
				//throw ex;
			}
			
		}
		
		
		public void run() {
			stillRunning = true;
			while (keeprunning) {
				send_to_server();	
				if (exitAfterSending && entries.size() == 0){
					keeprunning = false;
					continue;
				}
				try {
					if (entries.size() == 0 && !exitAfterSending)
						Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}	
			}
			stillRunning = false;
			
		}
		public void send_to_server () {
			while (entries.size() > 0) {
				StringsEntry e = null;
				synchronized (entries) {
					if (entries.size() > 0)
						e = entries.pop();
				}
				if (e == null){
					continue;
				}
				try {
					Pipeline pipe = redis_con.pipelined();
					pipe.sadd(get_addrs_key(), unsigned_long_str(e.addr));
					pipe.sadd(get_strings_set_key(), e.string);
					pipe.hset(get_stringbyaddrs_key(), unsigned_long_str (e.addr), e.string);
					pipe.sadd(get_stringbystr_key(e.string), unsigned_long_str (e.addr));
					pipe.sync();
					
				} catch (Exception ex) {
					if (e != null) {
						synchronized (entries) {
							entries.push(e);
						}
					}
					try {
						sleep(2);
					} catch (InterruptedException e1) {
						// dont care, just want to sleep
					}
					reconnect();
				}
				
			}
		}
	}
	
	class EntrytoSubmit {
		public long addr;
		public String string; 
		
		public EntrytoSubmit(long addr, String string) {
			this.addr = addr;
			this.string = string;
		}
		public EntrytoSubmit(StringsEntry e) {
			this.addr = e.addr;
			this.string = e.string;
		}
	}
	
	public String get_strings_base_key() {
		if (this.namespace.length() == 0)
			return REDIS_STRINGS;
		return this.namespace+":"+REDIS_STRINGS;
	}
	public String get_strings_set_key(){
		return get_strings_base_key()+":"+REDIS_STRINGS_SET_KEY;
	}
	
	public String get_addrs_key(){
		return get_strings_base_key()+":"+REDIS_ADDRS_SET_KEY;
	}
	
	public String get_stringbyaddrs_key(){
		return get_strings_base_key()+":"+REDIS_STRING_BY_ADDR_HSET;
	}
	
	public String get_stringbystr_key(String string){
		return get_strings_base_key()+":"+REDIS_STRING_BY_STR_HSET + ":" + string;
	}
	

	public String unsigned_long_str (long val) {
		return UnsignedLong.valueOf(val).toString();
	}
	
	public void addEntry (StringsEntry e) {
		synchronized (sharedStack) {
			sharedStack.push(e);
		}
	}

	
	public void wait_till_threads_complete() throws InterruptedException {
		
		for (RedisSubmit e: redis_sender_array) {
			e.exitAfterSending = true;
		}
		
		for (RedisSubmit e: redis_sender_array) {
			e.join();
		}
		
	}
	
	public void wait_till_threads_complete(ArrayList<Thread> threads) {
		
		for (Thread t : threads) {
			while (t.isAlive()) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}
		}
	}

	public void perform_string_read_send () throws IOException, InterruptedException {
		ArrayList<StringProcessor> threads = new ArrayList<StringProcessor>();
		Stack<String> entries = new Stack<String>();
		
		System.err.println("LOG starting background threads for string processing");
		for (int i = 0; i < MAX_STRING_PROC_T; i++){
			StringProcessor t = new StringProcessor(this, entries);
			threads.add(t);
			t.start();
		}
		long cnt = 0;
		System.err.println("LOG reading file");
		BufferedReader br = new BufferedReader(new FileReader(this.stringFile));
		String line;
		while ((line = br.readLine()) != null) {
		   if (line.length() > 5) {
			   entries.push(line);
			   cnt += 1;
		   }
		   if (cnt % 100000 == 0) {
			   System.err.println("LOG processed: "+cnt+" strings.");
		   }
		}
		br.close();
		System.err.println("LOG done reading files, waiting for processor threads to finish");
		for (StringProcessor s: threads) {
			s.exitAfterSending = true;
		}
		ArrayList<Thread> threads_thread = new ArrayList<Thread>();
		for (Thread s: threads) {
			threads_thread.add(s);
		}
		wait_till_threads_complete(threads_thread);
		System.err.println("LOG waiting for redis to finish");
		wait_till_threads_complete();
		
	}
	Stack<StringsEntry> sharedStack = new Stack<StringsEntry>();
	private ArrayList<RedisSubmit> redis_sender_array;
	private RedisSubmit redis_sender3;
	public StringsToRedis (String stringFile, String host, String port) throws IOException {
		this.stringFile = stringFile;
		redis_sender_array = new ArrayList<StringsToRedis.RedisSubmit>();
		for (int i = 0; i < MAX_REDI_SENDER; i++) {
			RedisSubmit e = new RedisSubmit(host, Integer.parseInt(port), sharedStack);
			redis_sender_array.add(e);
			e.start();
		}
				
		this.namespace = "default";
		//System.out.println("Initted me: "+this.range.toString());
	}
	
	public StringsToRedis (String stringFile, String host, String port, String namespace) throws IOException {
		this.stringFile = stringFile;
		redis_sender_array = new ArrayList<StringsToRedis.RedisSubmit>();
		for (int i = 0; i < MAX_REDI_SENDER; i++) {
			RedisSubmit e = new RedisSubmit(host, Integer.parseInt(port), sharedStack);
			redis_sender_array.add(e);
			e.start();
		}

		this.namespace = namespace;
		//System.out.println("Initted me: "+this.range.toString());
	}
	
	public static void do_strings(String[] args) throws IOException {
		if (args.length != 4) {
			System.err.println("ERROR!!\nStringsToRedis <strings_file> <redis_host> <redis_port> <redis_namespace>");
			System.err.println("Use Strings commend for fmt:\nstrings -t x -f java_dumps/*  > strings.txt");
			return;
		}
		
		String stringFile = args[0];
	    String host = args[1];
	    String port = args[2];
	    String namespace = args[3];
	    
	    StringsToRedis strToRedis = new StringsToRedis(stringFile, host, port, namespace);
	    try {
			strToRedis.perform_string_read_send();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		System.out.println("Done.");
		
	}
	public static void main(String[] args) throws IOException {
		do_strings(args);
	}
	
}
