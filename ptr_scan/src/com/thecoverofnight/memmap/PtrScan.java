//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
package com.thecoverofnight.memmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Transaction;

import com.google.common.primitives.UnsignedLong;
import com.thecoverofnight.memmap.StringsToRedis.RedisSubmit;

public class PtrScan {
	private static final long MAX_REDI_SENDER = 1;
	private static final String ERROR_LOG = "/home/dso/ptr_scan_errors";
	private static final String REDIS_SRC_RANGE_SET_KEY = "ranges";
	private static final String REDIS_SRC_RANGE_SET_SRCS = "srcs:all";
	private static final String REDIS_SRC_RANGE_SET_SINKS = "sinks:all";
	private static final String REDIS_HASH_SET_SINK_VALUE_KEY = "sinks:value";
	private static final String REDIS_HASH_SET_SRC_PTRS_KEY = "srcs:sinks";
	private static final String REDIS_SINKS_SET_KEY = "sinks";
	// use %s otherwise a negative output is possible, even though should be unsigned long 
	private static final String REDIS_SINKS_SRCS_SET_KEY = "sinks";
	private static final String REDIS_SRCS_SET_KEY = "srcs";
	private List<MemRange> ranges;
	private long r_addr;
	private boolean littleendian;
	private long end;
	private long offset;
	private int word_sz;
	private MemRange range;
	private long ptr_count;
	private String namespace;
	HashSet<Long> observedLongs = new HashSet<Long>();
	
	private RedisSubmit redis_sender;
	
	class RedisSubmit extends Thread{
		
		Stack<EntrytoSubmit> entries;// = new Stack<PtrScan.EntrytoSubmit>();
		boolean keeprunning = true;
		boolean exitAfterSending = false;
		private Jedis redis_con;
		boolean stillRunning = false;
		private String server;
		private int port;
		public RedisSubmit(String redis, int port, Stack<PtrScan.EntrytoSubmit> entries) {
			this.server = redis;
			this.port = port;
			connect();
			this.entries = sharedStack;
		}
		
		public void connect () {
			this.redis_con = new Jedis(server, port);
		}
		
		public void reconnect() {
			try {
				redis_con.shutdown();
			} catch (Exception ex) {
				// swallow this one, it does not mean much
			}
			try {
				connect ();
			} catch (Exception ex) {
				// this one matters (do
				ex.printStackTrace();
				keeprunning = false;
				stillRunning = false;
				exitAfterSending = false;
				// rethrow
				//throw ex;
			}
			
		}

		public void addEntry (EntrytoSubmit e) {
			synchronized (entries) {
				entries.push(e);
			}
		}
		
		public void run() {
			stillRunning = true;
			while (keeprunning) {
				send_to_server();	
				if (exitAfterSending && entries.size() == 0){
					keeprunning = false;
					continue;
				}
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}	
			}
			stillRunning = false;
			
		}
		public void send_to_server () {
			while (entries.size() > 0) {
				EntrytoSubmit e = null;
				synchronized (entries) {
					if (entries.size() > 0)
						e = entries.pop();
				}
				if (e == null)
					return;

				try {
					Pipeline pipe = redis_con.pipelined();
					pipe.sadd(get_srcs_set_key(), unsigned_long_str(e.src));
					pipe.sadd(get_sinks_set_key(), unsigned_long_str (e.sink));
					pipe.sadd(get_sinks_srcs_set_key(e.sink), unsigned_long_str (e.src));
					pipe.hset(get_hash_set_src_ptrs_key(), unsigned_long_str (e.src), unsigned_long_str (e.sink));
					pipe.hset(get_hash_set_sink_ptrs_key(), unsigned_long_str (e.sink), unsigned_long_str (e.value));
					pipe.sadd(get_set_range_sinks_key(e.sink_range), unsigned_long_str (e.sink));
					pipe.sadd(get_set_range_src_key(e.src_range), unsigned_long_str (e.src));
					pipe.sadd(get_set_range_key(), e.src_range);
					pipe.sadd(get_set_range_key(), e.sink_range);
					pipe.sync();
				} catch (Exception ex) {
					if (e != null) {
						synchronized (entries) {
							entries.push(e);
						}
					}
					try {
						// chances are there is a broken socket, so well back off
						sleep(1);
					} catch (InterruptedException e1) {
						// dont care, just want to sleep
					}
					// and then try to reconnect
					reconnect();
				}
				
			}
		}
	}
	
	class EntrytoSubmit {
		public long src;
		public long sink; 
		public long value; 
		public String src_range; 
		public String sink_range;
		
		public EntrytoSubmit(long src, long sink, long value, String src_range, String sink_range) {
			this.src = src;
			this.sink = sink;
			this.value = value;
			this.src_range = src_range;
			this.sink_range = sink_range;
		}
	}
	private ArrayList<RedisSubmit> redis_sender_array;
	private Stack<EntrytoSubmit> sharedStack;
	public PtrScan (String dumps_dir, String rangeAddr, String offset, String end, 
			String word_sz, String lilendian, String host, String port) throws IOException {
		sharedStack = new  Stack<PtrScan.EntrytoSubmit>();
		redis_sender_array = new ArrayList<PtrScan.RedisSubmit>();
		for (int i = 0; i < MAX_REDI_SENDER; i++) {
			RedisSubmit e = new RedisSubmit(host, Integer.parseInt(port), sharedStack);
			redis_sender_array.add(e);
			e.start();
		}
		this.ranges = MemRange.produce_ranges(dumps_dir);
		this.r_addr = UnsignedLong.valueOf(rangeAddr.replace("0x", ""), 16).longValue();
		this.offset = UnsignedLong.valueOf(offset.replace("0x", ""), 16).longValue();
		this.end = UnsignedLong.valueOf(end.replace("0x", ""), 16).longValue();
		this.word_sz = word_sz.equalsIgnoreCase("4") ? 4 : 8;
		this.littleendian = lilendian == "1" ? true : false;
		//this.redis_sender = new RedisSubmit(host, Integer.parseInt(port));
		//this.redis_sender.start();
		this.range = MemRange.find_value_range(r_addr, ranges);
		this.range.seek_to(this.offset);
		this.ptr_count = 0;
		this.namespace = "default";
		//System.out.println("Initted me: "+this.range.toString());
		
	}
	
	public PtrScan (String dumps_dir, String rangeAddr, String offset, String end, 
			String word_sz, String lilendian, String host, String port, String namespace) throws IOException {
		sharedStack = new  Stack<PtrScan.EntrytoSubmit>();
		redis_sender_array = new ArrayList<PtrScan.RedisSubmit>();
		for (int i = 0; i < MAX_REDI_SENDER; i++) {
			RedisSubmit e = new RedisSubmit(host, Integer.parseInt(port), sharedStack);
			redis_sender_array.add(e);
			e.start();
		}
		
		this.ranges = MemRange.produce_ranges(dumps_dir);
		this.r_addr = UnsignedLong.valueOf(rangeAddr.replace("0x", ""), 16).longValue();
		this.offset = UnsignedLong.valueOf(offset.replace("0x", ""), 16).longValue();
		this.end = UnsignedLong.valueOf(end.replace("0x", ""), 16).longValue();
		this.word_sz = word_sz.equalsIgnoreCase("4") ? 4 : 8;
		this.littleendian = lilendian.equalsIgnoreCase("1") ? true : false;
		//this.redis_sender = new RedisSubmit(host, Integer.parseInt(port));
		//this.redis_sender.start();
		this.range = MemRange.find_value_range(r_addr, ranges);
		this.range.seek_to(this.offset);
		this.ptr_count = 0;
		this.namespace = namespace;
		
		//System.out.println("Initted me: "+this.range.toString());
	}
	
	public byte[] bytes(long x) {
	    ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE);
	    buffer.putLong(x);
	    return buffer.array();
	}
	
	public byte[] bytes(String x) {
	    return x.getBytes();
	}
	
	public String get_namespace_key(){
		if (namespace == null || namespace.length() == 0)
			return "";
		return namespace+":";
		
	}
	
	public String get_srcs_set_key(){		
		return get_namespace_key()+REDIS_SRCS_SET_KEY;
	}

	public String get_sinks_set_key(){
		return get_namespace_key()+REDIS_SINKS_SET_KEY;
	}
	
	public String get_sinks_srcs_set_key(long sink){
		return get_namespace_key()+REDIS_SINKS_SRCS_SET_KEY+":"+unsigned_long_str (sink);
	}
	
	public String get_hash_set_src_ptrs_key(){
		return get_namespace_key()+REDIS_HASH_SET_SRC_PTRS_KEY;
	}
	
	public String get_hash_set_sink_ptrs_key(){
		return get_namespace_key()+REDIS_HASH_SET_SINK_VALUE_KEY;
	}
	
	public String get_range_key(String range) {
		return REDIS_SRC_RANGE_SET_KEY+":"+range;
	}
	
	public String get_set_range_sinks_key(String range){
		return get_namespace_key()+get_range_key(range)+":"+REDIS_SRC_RANGE_SET_SINKS;
	}
	
	public String get_set_range_src_key(String range){
		return get_namespace_key()+get_range_key(range)+":"+REDIS_SRC_RANGE_SET_SRCS;
	}

	public String unsigned_long_str (long val) {
		return UnsignedLong.valueOf(val).toString();
	}
	
	public String get_set_range_key(){
		return get_namespace_key()+REDIS_SRC_RANGE_SET_KEY;
	}
	
	public void wait_till_threads_complete() throws InterruptedException {
		
		for (RedisSubmit e: redis_sender_array) {
			e.exitAfterSending = true;
		}
		
		for (RedisSubmit e: redis_sender_array) {
			e.join();
		}
		
	}
	
	public void add_entry_for_redis (long src, long sink, long value, String src_range, String sink_range) {
		
		EntrytoSubmit e = new EntrytoSubmit(src, sink, value, src_range, sink_range);
		synchronized (this.sharedStack) {
			this.sharedStack.add(e);
		}
		
		
	}
	
	public void recurse_ptrs (long src, long sink, MemRange range) {
		//if (!redis_con.sismember("src", unsigned_long_str (src))) {
			recurse_ptrs (src, sink, range, 200);
		//}
	}
	
	public void recurse_ptrs (long src, long sink, MemRange range, int recurse_cnt) {
		String src_range = String.format("0x%08x-0x%08x", range.start, range.end);
		Long value = null;
		
		if (this.observedLongs.contains(src)) {
			return;
		}//else if (redis_con.sismember("src", unsigned_long_str (src))) {
		//	this.observedLongs.add(src);
		//	return;
		//}
		MemRange s_r = MemRange.in_ranges(sink, ranges, range);
		if (s_r != null) {
			String sink_range = String.format("0x%08x-0x%08x", s_r.start, s_r.end);
			long off = sink - s_r.start;
			if (word_sz == 4)
				value = s_r.read_dword_at_off(off);
			else
				value = s_r.read_qword_at_off(off);
			
			if (value != null && recurse_cnt > 0) {
				//System.out.println(String.format("src::0x%08x||sink::0x%08x||value::0x%08x", src, sink, value));
				this.observedLongs.add(src);
				add_entry_for_redis(src, sink, value, src_range, sink_range);
				//send_to_server(src, sink, value, src_range, sink_range);
				
				if (this.observedLongs.contains(sink)) {
					return;
				} else {//if (!redis_con.sismember("src", unsigned_long_str (sink))) {
					recurse_ptrs (sink, value, range, (recurse_cnt-1));
				}	
			}
		}
	}
	
	public void perform_ptr_scan() throws IOException, InterruptedException {
		long base = this.range.start;
		long end_loc = this.end == 0 ? this.range.fsize : this.end;
		long pos = offset;
		this.range.seek_to(offset);
		
		while (pos < end_loc - word_sz) {
			long src = pos + base;
			Long sink = null; 
			if (word_sz == 4)
				sink = this.range.read_dword_at_off(pos, littleendian);
			else
				sink = this.range.read_qword_at_off(pos, littleendian);
	        
			// walk the pointer if it is potentially a pointer to a pointer
	        if (sink != null){
		        try {
		        	recurse_ptrs (src, sink, this.range);
				} catch (Exception e) {
					String e_out_name = String.format("%s.%08x.txt", this.range.toString(), offset);
					String tb = e.getMessage()+"\n";
					for (StackTraceElement ele : e.getStackTrace()) {
						tb += ele.toString()+"\n";
					}
					String out_s = String.format("ERROR_START: src:0x%08x pos:0x%08x\n%s\nERROR_END\n", src, pos, tb);
					try {
						FileOutputStream e_out = new FileOutputStream(new File(ERROR_LOG, e_out_name), true);
						e_out.write(out_s.getBytes());						
					} catch (Exception e1) {
						// wtf??
					}
				}   
	            /*
	        		e_out = open(os.path.join(ERROR_LOG, range_str+".%08x.txt"%offset), "a")
	                tb = traceback.format_exc(sys.exc_info())
	                out_s = "ERROR_START: src:0x%08x pos:0x%08x\n%s\nERROR_END"%(src, pos, tb)
	                e_out.write(out_s)
	              */
	        } else {
	            break;
	        }
	        
	        // experiment with this to find pointers that are not aligned.
	        pos += this.word_sz;
	        try {
				this.range.seek_to(pos);
			} catch (IOException e) {
				String e_out_name = String.format("%s.%08x.txt", this.range.toString(), offset);

				String tb = e.getMessage()+"\n";
				for (StackTraceElement ele : e.getStackTrace()) {
					tb += ele.toString()+"\n";
				}
				
				String out_s = String.format("ERROR_START: src:0x%08x pos:0x%08x\n%s\nERROR_END\n", src, pos, tb);
				
				try {
					new FileOutputStream(new File(ERROR_LOG, e_out_name), true).write(out_s.getBytes());
				} catch (IOException e1) {
					// wtf?  cant do anything going to bail anyway
					//e1.printStackTrace();
				}
				break;
			}
		}
		//this.redis_sender.exitAfterSending = true;
		wait_till_threads_complete();
	}
	
	public static void do_ptr_scan(String[] args) throws IOException, InterruptedException {
	    if (args.length != 9) {
	    	System.err.println("ERROR\nptrscan <dumps_dir> <rangeAddr> <offset> <end> <word_sz> <littleendian> <host> <port> <namespace>");
			return;
		}

		//String basic_command = "java -jar ptr_scan /research_data/memory_forensics/java_ubuntu_custom2/java_dumps/ 0x93e9f000 0 0 4 1 10.16.121.14 6379";
	    String dumps_dir = args[0];
	    String rangeAddr = args[1];
	    String offset = args[2];
	    String end = args[3];
	    String word_sz = args[4];
	    String littleendian =args[5];
	    String host = args[6];
	    String port = args[7];
	    String namespace = args[8];
	    	    

		PtrScan ptrScan = new PtrScan(dumps_dir, rangeAddr, offset, end, word_sz, littleendian, host, port, namespace);
		ptrScan.perform_ptr_scan();
		System.out.println("Completed: "+rangeAddr+" found "+ptrScan.ptr_count + " pointers.");
		
	}
	public static void main(String[] args) throws IOException, InterruptedException {
		do_ptr_scan(args);
	}
	    
	
}
